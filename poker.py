# LIBRARIES
import random as rd

## VARS

defaultStack = 500

global pot
pot = 0

players = [] #Liste tuples (ID, playername)
playernames = [] #Liste des playernames
ingamePlayers = [] #Liste des joueurs qui ne se sont pas couchés

deck = [] #List containing each cards of the deck
communityCards = [] #List containing the community cards
stacks = [] #Liste des tapis de chaque joueurs (ID, amount)
cards = [] #Liste des cartes de chaques joueurs (ID, cardList)
bets = [] #List des bets de chaques choueurs (ID, bet)

smallBlindID = 0
bigBlindID = 0

finishedRound = False

values = ['2','3','4','5','6','7','8','9','X','J','Q','K','A']
colors = ['C','S','H','D']

colors_names = ["Trèfle","Pic","Coeur","Carreau"]
values_names = ["Deux", "Trois", "Quatre", "Cinq", "Six", "Sept", "Huit", "Neuf", "Dix", "Valet", "Dame", "Roi","As"]


## PLAYERS

def addPlayer(playername, stack = defaultStack):
    """
        Add a player to the game.

        Add a player to the game and assigns him an ID. 
        Check wether the username is already taken or not.
        Possibility to set the initial stack of the player (empty for default).

        :param playername: Name of the player added\n
        :param stack: (Optional) Stack of the player
        :type playername: str
        :type stack: int
        :return: Success
        :rtype: bool
    """
    
    if(not playernameExists(playername)):

        #On ajoute un joueur
        playerID = len(players) + 1
        players.append((playerID, playername))
        playernames.append(playername)
        setStack(playerID, stack)

        print("Joueur ajouté: " + playername + " | ID: #" + str(playerID) + " | Tapis: " + str(getStack(playerID)))

        return True

    print("Ce nom est déjà pris !")

    return False

def playernameExists(playername):
    """
        Check wether the name is already taken. (case insensitive)

        :param playername: Name to check
        :type playername: str
        :return: Answer to the name check
        :rtype: bool
    """

    for name in playernames:

        if(name.lower() == playername.lower()):
            return True
    
    return False

def getPlayername(playerID):
    """
        Return the playername assigned to an ID

        :param playerID: ID of the player
        :type playerID: int
        :return playername: Name of the player
        :rtype: str
    """

    player = getValueByKey(playerID, players)

    if(player != None):
        return player[1]

    return ""

def getNextID(playerID):

    if(playerID == len(players)):
        return 1
    else:
        return playerID + 1

def getNextPlayer(playerID):
    """
        Returns the following player's ID in the player list

        :param playerID: ID of the current player
        :type playerID: int
        :return nextPlayerID: ID of the following player in the list
        :rtype: int
    """

    nextPlayerID = getNextID(playerID)

    while(nextPlayerID not in set(ingamePlayers)):
        nextPlayerID = getNextID(nextPlayerID)
    
    return nextPlayerID

def getPreviousPlayer(playerID):
    """
        Returns the previous player's ID the player list

        :param playerID: ID of the current player
        :type playerID: int
        :return previousPlayerID: ID of the previous player in the list
        :rtype: int
    """

    if(playerID == ingamePlayers[0]):
        #Go to the end of the list
        return ingamePlayers[-1]
    else:
        return ingamePlayers[ingamePlayers.index(playerID) - 1]

def getStack(playerID):
    """
        Get the current stack of a player

        Returns -1 if the player is not in the list

        :param playerID: ID of the player
        :type playerID: int
        :return: Player's stack
        :rtype: int
    """

    stack = getValueByKey(playerID, stacks)

    if(stack != None):
        return stack[1]
    
    return -1

def setStack(playerID, stack):
    """
        Set the stack of a player

        If the player is not already registered in the list, it adds it,
        if the player is already in it, it edits the list.

        :param playerID: ID of the player
        :param stack: Stack to set to the player
        :type playerID: int
        :type stack: int
    """

    if(getStack(playerID) == -1):
        stacks.append((playerID, stack))
    else:
        index = getIndexByKey(playerID, stacks)
        stacks[index] = (playerID, stack)

def displayGameState(currentPlayerID = 0):
    global pot
    
    print("\n----------------------------------------------------")
    print("          Etat de la partie")

    #Community cards
    if(len(communityCards) > 0):
        
        displayString = "\n Cartes communes : \n"

        for card in communityCards:

            displayString += " [" + getCardName(card) + "] "
    
        print(displayString)

    else:

        print("\n Il n'y a pas encore de cartes sur la table. (pre-flop)")

    #Personal cards
    if(currentPlayerID != 0):

        displayString = "\n Vos cartes: \n"

        for card in getCards(currentPlayerID):

            displayString += " [" + getCardName(card) + "] "
        
        print(displayString)
        
        print("\n Meilleur main: {hand}".format(hand = getBestHand(currentPlayerID)))    
    
        previousPlayerID = getPreviousPlayer(currentPlayerID)
        previousBet = getBet(previousPlayerID)
        if(previousBet != -1):
            print("\n Mise du joueur précédent (" + str(getPlayername(previousPlayerID)) + "): " + str(previousBet) + "$")
        elif(previousBet == 0):
            print("\n Le joueur précédent a check.")
        else:
            print("\n Vous commencez le tour de bet.")
    
    print("\n Pot: " + str(pot) + "$")

    print("\n          ---------------------")

    for player in players:

        playerID = player[0]

        prefix = "\n"
    
        if(smallBlindID == playerID):
            prefix += "SB "
        elif(bigBlindID == playerID):
            prefix += "BB "
        else:
            prefix += "   "

        if(playerID not in set(ingamePlayers)):
            prefix += "Folded "
        else:
            prefix += "       "

        if(currentPlayerID == playerID):
            prefix += ">"
        else:
            prefix += " "

        bet = getBet(playerID)

        if(bet == -1):
            print("{prefix}{playername} | N'a pas encore bet | Tapis: {stack}$".format(prefix = prefix, playername = getPlayername(playerID), stack = str(getStack(playerID))))
        elif(bet == 0):
            print("{prefix}{playername} | Check | Tapis: {stack}$".format(prefix = prefix, playername = getPlayername(playerID), stack = str(getStack(playerID))))
        else:
            print("{prefix}{playername} | Bet: {bet}$ | Tapis: {stack}$".format(prefix = prefix, playername = getPlayername(playerID), bet = str(getBet(playerID)), stack = str(getStack(playerID))))

    print("\n----------------------------------------------------")

def setRoles():
    """
        Set the roles to players IDs
    """
    
    global smallBlindID
    global bigBlindID

    if(smallBlindID == 0):
        #First game => random
        smallBlindID = rd.choice(ingamePlayers)
    else:
        smallBlindID = getNextPlayer(smallBlindID)
    
    bigBlindID = getNextPlayer(smallBlindID)

    print("Petite blinde: " + getValueByKey(smallBlindID, players)[1])
    print("Grosse blinde: " + getValueByKey(bigBlindID, players)[1])

## CARDS

def createDeck():
    """
        Create the game deck
    """

    values = ['2','3','4','5','6','7','8','9','X','J','Q','K','A']
    colors = ['C','S','H','D']

    for v in values:
        for c in colors:
            deck.append((v,c))
    
    rd.shuffle(deck)
    

def drawCard():
    """
        Draw a card from the deck and removes it

        :return card: Returns a card
        :rtype: tuple
    """

    card = rd.choice(deck)
    deck.remove(card)

    return card
 

def dealCards():
    """
        Deal 2 cards to every player in the ingamePlayers list
    """

    if(len(deck) >= 2*len(ingamePlayers)):
        for p in ingamePlayers:
            c = []
            c.append(drawCard())
            c.append(drawCard())
            setCards(p, c)

def getCards(playerID):
    """
        Get the current cards of a player

        :param playerID: Id of the player
        :type playerID: int
        :return: List of the cards
        :rtype: list
    """

    cardList = getValueByKey(playerID, cards)

    if(cardList != None):
        return cardList[1]
    
    return []

def setCards(playerID, cardList):
    """
        Set the cards of a player

        If the player is not already registered in the list, it adds it,
        if the player is already in it, it edits the list.

        :param playerID: ID of the player
        :param cards: List of the cards to set to a player
        :type playerID: int
        :type cards: list
    """

    if(getCards(playerID) == []):
        cards.append((playerID, cardList))
    else:
        index = getIndexByKey(playerID, cards)
        cards[index] = (playerID, cardList)

def getCardName(card):
    """
        Returns the name of a card

        :param card: Card
        :type card: tuple
        :return name: Name of the card
        :rtype: str
    """
    global values, colors, colors_names, values_names

    return values_names[values.index(card[0])] + " de " + colors_names[colors.index(card[1])]

#COMPARER LES MAINS

def getCardIndexValue(strValue):
    global values

    if(strValue in set(values)):
        return values.index(strValue)

def getHighestCard(cardList):
    print("Ta mere")

def getBestHand(playerID):
    """
        Returns the list of the best hand for a player
    """
    global communityCards
    availableCards = getCards(playerID) + communityCards

    global values

    bestHand = "High Card"

    if(hasQF(availableCards)):
        if(hasQFR(availableCards)):
            #QFR
            bestHand = "Quinte Flush Royale"
            
        else:
            #QF
            bestHand = "Quinte Flush"
                
    elif(hasMultiple(4, availableCards)[0]):
        
        bestHand = "Carré"

    elif((hasMultiple(3, availableCards)[0] and hasMultiple(2, availableCards)[0]) or len(hasMultiple(3, availableCards)[1])==2):

        bestHand = "Full"

    elif(hasColor(availableCards)):

        bestHand = "Color"
    
    elif(hasQuinte(availableCards)[0]):
        
        listQ = hasQuinte(availableCards)[1]
        bestQ = listQ[0]

        for i in range(len(listQ)):
            if(getCardIndexValue(listQ[i][0]) > getCardIndexValue(bestQ[0])):
                bestQ = listQ[i]
        
        bestHand = "Quinte"

    elif(hasMultiple(3, availableCards)[0]):

        bestHand = "Brelan"
    
    elif(len(hasMultiple(2, availableCards)[1]) >= 2):

        bestHand = "Double Paire"
    
    elif(len(hasMultiple(2, availableCards)[1]) == 1):
        
        bestHand = "Paire"

    return bestHand

def hasQFR(availableCards):

    global colors

    for color in colors:
        
        if(('A', color) in set(availableCards) and ('K', color) in set(availableCards) and ('Q', color) in set(availableCards) and ('J', color) in set(availableCards) and ('X', color) in set(availableCards)):
            
            return True
        
    return False

def hasQF(availableCards):
    
    availableValues = [c[0] for c in availableCards]

    if('A' in set(availableValues)):
        if('K' in set(availableValues)):
            if('Q' in set(availableValues)):
                if('J' in set(availableValues)):
                    if('X' in set(availableValues)):

                        return True

    return False

def hasMultiple(n, availableCards):
    
    availableValues = [c[0] for c in availableCards]

    multiples = []

    for v in availableValues:

        if(availableValues.count(v) == n):
            multiples.append(v)

            #Remove the cards to avoid redondancy
            for i in range(n):
                availableValues.remove(v)
    
    if(len(multiples) > 0):
        return True, multiples
    else:
        return False, []


def hasColor(availableCards):
    global colors

    availableColors = [c[1] for c in availableCards]

    for color in colors:
        if(availableColors.count(color) >= 5):
            return True , color

    return False

def hasQuinte(availableCards):
    global values

    availableValues= [c[0] for c in availableCards]

    listQ = []

    for value in availableValues:
    
        index = values.index(value)
        if(index<=8):
        
            if(values[index + 1] in set(availableValues)):
                if(values[index + 2] in set(availableValues)):
                    if(values[index + 3] in set(availableValues)):
                        if(values[index + 4] in set(availableValues)):
                            listQ.append([values[index+n] for n in range(4)])
    
    if(listQ != []):
        return True, listQ
    else:
        return False, []

## BETS

def getBet(playerID):
    """
        Get the bet of a player

        :param playerID: ID of the player
        :type playerID: int
        :return bet: Bet amount of the player
        :rtype: int
    """

    bet = getValueByKey(playerID, bets)

    if(bet != None):
        return bet[1]

    return -1

def setBet(playerID, amount):
    """
        Set the bet of a player (0 = check) in the bets list

        If the player is not already registered in the list, it adds it,
        if the player is already in it, it edits the list.

        :param playerID: ID of the player
        :param amount: Amount of the bet
        :type playerID: int
        :type amount: int
    """

    global pot

    if(getBet(playerID) == -1):
        bets.append((playerID, amount))
        pot += amount
        setStack(playerID, getStack(playerID) - amount)
    else:
        index = getIndexByKey(playerID, bets)
        diff = amount - bets[index][1]
        pot += diff
        bets[index] = (playerID, amount)
        setStack(playerID, getStack(playerID) - diff)
       
## GENERAL

def startGame():
    """
        Start the game.

        Create a new deck and add players to the ingamePlayers list.
        Also assign small blind and big blind
    """

    #Clear all list etc.
    deck.clear()
    communityCards.clear()
    cards.clear()
    bets.clear()
    ingamePlayers.clear()

    global smallBlindID, bigBlindID, finishedRound, pot
    smallBlindID = 0
    bigBlindID = 0
    finishedRound = False
    pot = 0


    for p in players:
        ingamePlayers.append(p[0])
    
    gameID = rd.randint(10e4, 10e5-1)

    print("\n----------------------------------------")
    print("             Partie #" + str(gameID))
    print("----------------------------------------\n")

    createDeck()
    dealCards()
    setRoles()

    #Little and big blind
    setBet(smallBlindID, 1)
    setBet(bigBlindID, 2)

def startBettingRound():
    """
        Initiate a betting round
        Start with the player after smallBlind
    """
    global finishedRound

    currentPlayer = getNextPlayer(bigBlindID)
    firstPlayer = True

    while((not isBettingRoundOver() or firstPlayer == True) and not finishedRound):
        clearConsole()
        nextPlayerReady(currentPlayer)
        clearConsole()
        nextPlayer = getNextPlayer(currentPlayer) #If a player folds, the next player is already registered
        askAction(currentPlayer, firstPlayer)

        if(len(ingamePlayers) == 1):
            finish(ingamePlayers[0])

        firstPlayer = False
        currentPlayer = nextPlayer
    
    #Clear bets when it's over
    bets.clear()
    #LOG

def isBettingRoundOver():
    """
        Checks if the betting round can be ended

        :return finished: Returns true or false wether the bets are equals or not
        :rtype: bool
    """

    for playerID in ingamePlayers:
        
        if(getBet(playerID) != getBet(getPreviousPlayer(playerID))):
            #Si toutes les mises ne sont pas égales
            return False
  
    return True

def nextPlayerReady(playerID):
    """
        Ask if the next player is ready to play
    """
    
    print("Prochain joueur : {playername}".format(playername = getPlayername(playerID)))
    input("Appuyez sur ENTRER pour jouer")
    return True

def askAction(playerID, firstBetter):
    """
        Ask what action a player wants to execute

        :param playerID: ID of the player
        :type playerID: int
    """
    
    playername = getPlayername(playerID)
    print("C'est au tour de " + playername + " (#" + str(playerID) + ")")

    displayGameState(playerID)
    
    availableActions = ["fold", "raise"]
    if(getBet(getPreviousPlayer(playerID)) <= 0):
        availableActions.append("check")
    else:
        availableActions.append("call")

    actionSuccess = False
    while actionSuccess == False:
        print("\nVous pouvez :")
        for action in availableActions:
            print("    - " + action)

        print("\nAction a effectuer: ")
        inp = input("=> ")

        cmd = []
        if(inp.strip() != ''):
            cmd = inp.lower().split(' ')

        if(len(cmd) >= 1 and cmd[0] in set(availableActions)):
            
            if(cmd[0] == "raise" and len(cmd) == 2):
                try:
                    amount = int(cmd[1])
                    if(amount <= 0):
                        print("Veuillez renseigner un montant supérieur à 0")
                    else:
                        actionSuccess = playerAction(playerID, "raise", amount)
                except:
                    print("Veuillez renseigner un montant après votre commande: raise [montant]")
            elif(len(cmd) == 1):
                actionSuccess = playerAction(playerID, cmd[0])
            else:
                print("Veuillez rentrer un montant pour raise: raise [montant]")

def playerAction(playerID, action, amount = 0):
    """
        Execute the action requested by the player

        :param playerID: ID of the player
        :param action: Action requested by the player
        :param amount: (Optional) amount of the new value if the player raises
        :type playerID: int
        :type action: str
        :type amount: int
        :return success: Success of the action
        :rtype: bool
    """

    if(playerID in set(ingamePlayers)):

        if(action == "call"):
            #Call
            previousBet = getBet(getPreviousPlayer(playerID))
            if(getStack(playerID) >= previousBet):

                setBet(playerID, previousBet)
                return True

            print("Vous n'avez pas les fonds nécessaires pour cela.")
            return False
            
        elif(action == "raise"):
            #Raise
            if(amount >= 2*getBet(getPreviousPlayer(playerID))):

                if(getStack(playerID) >= amount):

                    setBet(playerID, amount)
                    return True

                print("Vous n'avez pas les fonds nécessaires pour cela.")
                return False

            print("Vous devez au minimum doubler la mise.")
            return False
        
        elif(action == "fold"):
            #Fold
            ingamePlayers.remove(playerID)
            
            return True
        elif(action == "check"):
            #Check

            if(getBet(getPreviousPlayer(playerID)) <= 0):
                setBet(playerID, 0)
                return True

            return False
        else:
            print("Error, please retry")
            return False
    
    print("Vous n'êtes pas autorisé à jouer.")
    return False

def finish(winnerID = 0):

    global finishedRound

    clearConsole()

    if(finishedRound == False):
        
        if(winnerID == 0):
            #Compare hands
            #Search the winner
            #winner = ID
            print("Fin de partie (1)")
        else:
            print("Fin de la partie (2)")
        
        print("{playername} gagne la partie !".format(playername = getPlayername(winnerID)))

        finishedRound = True

        displayGameState()

## TOOL FUNCTIONS
  
def getValueByKey(key, searchList):

    for item in searchList:
        if(item[0] == key):
            return item

    return None

def getIndexByKey(key, searchList):

    index = 0

    for item in searchList:
        if(item[0] == key):
            return index
        
        index += 1
    
    return None

def showVariables():
    """
        Display in the console every global variables. (Debug only)
    """

    print("\n=== Variables ===")
    print("\nplayers :")
    print(players)
    print("\nstacks :")
    print(stacks)
    print("\ncards :")
    print(cards)
    print("\ncommunityCards :")
    print(communityCards)
    print("\nplayernames :")
    print(playernames)
    print("\ndeck:")
    print(deck)
    print("\npot:")
    print(pot)

def clearConsole(lines = 50):
    for l in range(lines):
        print(' ')

## GAME

### Get the total player number

clearConsole()

print("""
######   #####  ##   ## ####### ######
##   ## ##   ## ##   ## ##      ##   ##
##   ## ##   ## ##  ##  ##      ##   ##
######  ##   ## #####   #####   ###### 
##      ##   ## ##  ##  ##      ##   ##
##      ##   ## ##   ## ##      ##   ##
##       #####  ##   ## ####### ##   ##
""")

clearConsole(10)

nPlayer = 0
while(nPlayer <= 1):
    inp = input("Nombre de joueurs (au moins 2): ")
    
    try:
        nPlayer = int(inp)
    except:
        nPlayer = 0
        print("Veuillez rentrer une valeur valide !")

for i in range(1, nPlayer+1):
    
    addedPlayer = False
    while(addedPlayer == False):
        print("Nom du joueur #" + str(i))
        inp = input("=> ")
        addedPlayer = addPlayer(inp)

## Game loop

def gameLoop():
    clearConsole()
    startGame()
    
    #Pre-flop
    startBettingRound()

    #Flop
    drawCard() #Brule une carte
    communityCards.append(drawCard())
    communityCards.append(drawCard())
    communityCards.append(drawCard())

    startBettingRound()

    #Turn
    drawCard()
    communityCards.append(drawCard())
    
    startBettingRound()

    #River
    drawCard()
    communityCards.append(drawCard())

    startBettingRound()

    finish()

    clearConsole(10)

    print("On continue ? (Oui/Non)")
    while(True):
        inp = input("(Oui/Non) > ").strip().lower()
        
        if(inp == "oui" or inp == 'o' or inp == ''):
            gameLoop()
            break
        elif(inp == "non" or inp == "n"):
            break

gameLoop()